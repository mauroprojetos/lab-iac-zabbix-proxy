# Laborátorio IAC - Implementação Zabbix Proxy

## Descrição
Esse projeto tem como objetivo implementar um Zabbix Proxy na AWS com IAC. Em uma outra versão a ideia é expandir para outros cloud providers.

## Tabela de Conteúdos
- [Requisitos](#requisitos)
- [Como Usar](#como-usar)
- [Contribuição](#contribuição)
- [Licença](#licença)

## Requisitos

É necessário uma conta na AWS e ter criado VPC, subnet e security groups para alteração no arquivo variable.tf. Também é necessário criar uma credencial no IAM da AWS e definir as variáveis AWS_ACCESS_KEY_ID e AWS_SECRET_ACCESS_KEY para o terraform conseguir criar os recursos.

Ter uma conta no gitlab para ser possível clonar o projeto e assim rodar o projeto no seu ambiente utilizando o gitlab-ci.

## Como Usar

1 - Crie um fork do projeto na sua conta gitlab.

2 - Em Settings > CI/CD > Variables, defina as variáveis AWS_ACCESS_KEY_ID e AWS_SECRET_ACCESS_KEY com os valores da credencial criada na AWS.

3 - Altere o arquivo variables.tf com os valores de Security Group e Subnet da sua VPC criada.

4 - Para esse projeto estou usando a zona us-east-1. Se quiser alterar é necessário mudar no arquivo main.tf

5 - É necessário criar um bucket no S3 com o nome "lab-zabbix-proxy" ou alterá-lo no arquivo main.tf. Isso serve para armazenar o state do terraform e garantir a idempotência.

6 - Execute a pipeline no gitlab-ci e veja a mágica acontecer.

## Contribuição
Para este projeto contei com a ajuda do https://www.terraform-best-practices.com/ e meu amigos ChatGPT e Bard!
https://www.youtube.com/watch?v=bcaqaeO1xTc

## Licença
You are free!
