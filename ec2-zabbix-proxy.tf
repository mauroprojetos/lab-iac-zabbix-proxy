resource "aws_key_pair" "this" {
  key_name   = "alohomora"
  public_key = var.ec2_keypair
}

resource "aws_instance" "this" {

  ami                    = var.ec2_ami
  instance_type          = var.ec2_instance_type
  vpc_security_group_ids = [var.ec2_sg]
  subnet_id              = var.ec2_subnet
  key_name               = aws_key_pair.this.key_name
  depends_on             = [aws_key_pair.this]

  tags = {
    owner      = "emiliano"
    managed_by = "terraform"
    Name       = "zabbix-proxy-empresa"

  }
}