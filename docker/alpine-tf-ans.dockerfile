FROM alpine:latest

RUN apk update
RUN apk add curl wget unzip python3 py3-pip
RUN pip3 install pip --upgrade
RUN pip3 install ansible
RUN wget https://releases.hashicorp.com/terraform/1.5.7/terraform_1.5.7_linux_amd64.zip
RUN unzip terraform_1.5.7_linux_amd64.zip
RUN mv terraform /usr/bin/terraform

RUN rm -rf terraform*.zip
RUN apk del curl wget unzip