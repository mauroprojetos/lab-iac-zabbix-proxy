variable "ec2_keypair" {
  description = "Contém o valor da chave ssh do EC2"
  type        = string
  default     = ""
}

variable "ec2_ami" {
  description = "Contém o valor da ami do ubuntu 22.04 LTS"
  type        = string
  default     = "ami-053b0d53c279acc90"
}

variable "ec2_instance_type" {
  description = "Contém o valor do tipo de instância EC2"
  type        = string
  default     = "t3.micro"
}

variable "ec2_sg" {
  description = "Contém o valor do Security Group"
  type        = string
  default     = "sg-12dbf60d"
}

variable "ec2_subnet" {
  description = "Contém o valor da Subnet"
  type        = string
  default     = "subnet-6c34110a"
}