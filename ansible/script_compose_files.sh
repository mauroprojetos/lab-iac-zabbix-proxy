# Create zabbix working directory

mkdir -p ~/zabbix/env_vars

# Create Zabbix Docker Compose env files

echo "MYSQL_USER=zabbix
MYSQL_PASSWORD=zabbix
MYSQL_ROOT_PASSWORD=root_pwd
MYSQL_DATABASE=zabbix_proxy" > ~/zabbix/env_vars/.env_db_mysql_proxy

echo "ZBX_HOSTNAME=zabbix-proxy-mysql" > ~/zabbix/env_vars/.env_prx_mysql

echo "MYSQL_USER_FILE=/run/secrets/MYSQL_USER
MYSQL_PASSWORD_FILE=/run/secrets/MYSQL_PASSWORD
MYSQL_ROOT_PASSWORD_FILE=/run/secrets/MYSQL_ROOT_PASSWORD
MYSQL_DATABASE=zabbix" > ~/zabbix/env_vars/.env_db_mysql

echo "zabbix" > ~/zabbix/env_vars/.MYSQL_USER
echo "zabbix" > ~/zabbix/env_vars/.MYSQL_PASSWORD
echo "root" > ~/zabbix/env_vars/.MYSQL_ROOT_USER
echo "root_pwd" > ~/zabbix/env_vars/.MYSQL_ROOT_PASSWORD

touch ~/zabbix/env_vars/.env_prx
